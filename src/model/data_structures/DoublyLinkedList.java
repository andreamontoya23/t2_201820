package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	
	
	Integer getSize();
	public void addFirst();
	public void append();
	public void removeFirst();
	public void remove (int pos);
	public T get ();
	public int size();
	public boolean isEmpty();

}
